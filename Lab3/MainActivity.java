package com.example.hp.currencyconverter;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

        private Button convButtonv1;
        private EditText editTextv1;
        private TextView textViewv1;
        private String USD;

        // URL used to extract real-time currency exchange rates, using JSON for the Petrol-AirCra
        private static String url = "https://api.fixer.io/latest?base=USD";

        //Variable to stroe the buffered Json string JSON obj extracted from website
        String json = "";

        //this string will be assigned ea line of the jason string in a loop usin ggthe

        String line = "";

        //variable that will obtain the value we need for currency conversion
        String rate = "";

    @Override
    // java fcn here
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextv1 = findViewById(R.id.EditTextv1);
        convButtonv1 = findViewById(R.id.convButton);
        textViewv1 = findViewById(R.id.Yen);

        convButtonv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertTOYen)
            {

                // all 'system.out' messages are a great way
                // yo ucan calso output the value of key vars or methods in system
                // assists in debugging and trouble shotting errors in your coe until you become more familiar with android
                System.out.println("\nTESTING 1 ... before AsynchExecution\n");

                BackgroundTask object = new BackgroundTask();

                object.execute();



            }
        });
    }
    private class BackgroundTask extends AsyncTask<Void, Void, String>
    {
        @Override
        protected void onPreExecute(){ super.onPreExecute(); }
        @Override
        protected void onProgressUpdate(Void... values){super.onProgressUpdate(values);}
        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);
            System.out.println("\nWhat is rate: " + result + "\n");
            Double value = Double.parseDouble(result);

            System.out.println("\nTesting JSON String" + value);

            USD = editTextv1.getText().toString();
            if (USD.equals("")) {
                textViewv1.setText("NO BLANK INPUTS");
            } else {
                Double convInputToDbl = Double.parseDouble(USD);
                Double output = convInputToDbl * value;
                textViewv1.setText("$" + USD + " = " + "¥" + String.format("%.2f", output));
                editTextv1.setText("");
            }
        }

        @Override
        protected String doInBackground(Void... params)
        {
            try
            {
                URL web_url = new URL(MainActivity.this.url);

                HttpURLConnection httpURLConnection = (HttpURLConnection)web_url.openConnection();
                httpURLConnection.setRequestMethod("GET");

                System.out.println("\nTesting ... BEFORE connection method to URL\n");

                // invoke
                httpURLConnection.connect();

                InputStream inputStream = httpURLConnection.getInputStream();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                System.out.println("CONNECTION SUCCESSFUL\n");
                while(line != null)
                {
                    line = bufferedReader.readLine();
                    json += line;
                }
                System.out.println("\nTHE JSON: " + json);

                JSONObject obj = new JSONObject(json);

                JSONObject objRate = obj.getJSONObject("rates");

                rate = objRate.get("JPY").toString();
            }
            catch(MalformedURLException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (JSONException e)
            {
                Log.e("MYAPP", "unexpected JSON exception", e);
                System.exit(1);
            }
            return rate;
        }

    }
}
