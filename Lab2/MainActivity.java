package com.example.hp.mycurrencyconverter_040818;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    // TODO:  Dollar to Yen Conversion
    // NEED:  TextView(Instruction), EditText(Input), Button(Compute), TextView(Output)
    // Conversion Rate:  $1 = 106.34 Y
    // NEED:  Push Button (btn_covert_currency) to Compute
    // REJECT:  Null or Non-Numeric Input

    private Button btn_convert_currency;
    private EditText editText_dollar_input;
    private TextView textView_Yen_amt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize LAYOUT elements
        editText_dollar_input = findViewById(R.id.editText_dollar_input);
        btn_convert_currency = findViewById(R.id.btn_covert_currency);
        textView_Yen_amt = findViewById(R.id.textView_Yen_amt);

        btn_convert_currency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dollar_input = editText_dollar_input.getText().toString();
                convert_dollar_to_yen(dollar_input);
            }
        });
    }

    private void convert_dollar_to_yen(String s){
        if(s.equals("")){
            textView_Yen_amt.setText("Input Cannot be Blank!");
        } else {
            try{
                Double dollar_amt = Double.parseDouble(s);
                Double yen_amt = dollar_amt * 106.34;
                //textView_Yen_amt.setText(yen_amt.toString());
                textView_Yen_amt.setText("$ " + dollar_amt + " = ¥" + String.format("%.2f",yen_amt));
            } catch(NumberFormatException e){
                e.printStackTrace();
                textView_Yen_amt.setText("Input Must be Number Only!");
            }
        }
    }
}
